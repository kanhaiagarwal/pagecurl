//
//  AudioViewController.swift
//  PageCurl
//
//  Created by Kumar Agarwal, Kanhai on 03/06/18.
//  Copyright © 2018 Kumar Agarwal, Kanhai. All rights reserved.
//

import UIKit
import AVFoundation
import LKAWaveCircleProgressBar

class AudioViewController: UIViewController, AVAudioPlayerDelegate {

    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var playPauseButton: UIButton!
    @IBOutlet weak var currentTime: UILabel!
    @IBOutlet weak var totalTime: UILabel!
    @IBOutlet weak var topContainer: UIView!
    
    var audioPlayer:AVAudioPlayer = AVAudioPlayer()
    var playState : Bool = false
    var progress : LKAWaveCircleProgressBar = LKAWaveCircleProgressBar()
    
    override func viewDidLoad() {
        
        
        
        super.viewDidLoad()
        
        do {
            
            let audioPath = Bundle.main.path(forResource: "tareefan", ofType: "mp3")
            try audioPlayer = AVAudioPlayer(contentsOf: URL(fileURLWithPath: audioPath!))
            audioPlayer.delegate = self
            
        } catch {
           print(error.localizedDescription)
        }
        
        progress = LKAWaveCircleProgressBar(frame: CGRect(x: 0, y: 0, width: 3 * topContainer.frame.width / 4, height: 3 * topContainer.frame.width / 4))
        
        print("progress.x: \(progress.frame.origin.x), progress.y: \(progress.frame.origin.y)")
        
        progress.setProgress(slider.value, animated: true)
        
        progress.center.x = view.center.x
        progress.center.y = topContainer.center.y
        print("progress.x: \(progress.frame.origin.x), progress.y: \(progress.frame.origin.y)")
        
        view.addSubview(progress)
        
        // Set the max value of the slider equal to the duration of the audio.
        slider.maximumValue = Float(audioPlayer.duration)
        
        // Set the totalTime label to the max duration of the audio
        totalTime.text = getTime(time: audioPlayer.duration)
        
        // Call updateSlider every 0.1 seconds.
        _ = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.updateSlider), userInfo: nil, repeats: true)
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        playState = false
        playPauseButton.setTitle("Play", for: .normal)
        
    }
    
    // Change the audio state to pause or play, based on the current value.
    @IBAction func onButtonPressed(_ sender: UIButton) {
        
        if playState == true {
            playState = false
            audioPlayer.pause()
            playPauseButton.setTitle("Play", for: .normal)
        } else {
            playState = true
            audioPlayer.play()
            playPauseButton.setTitle("Pause", for: .normal)
        }
    }
    
    // Method to change the audio based on the current state of the slider.
    @IBAction func changeAudioTime(_ sender: UISlider) {
        audioPlayer.stop()
        audioPlayer.currentTime = TimeInterval(slider.value)
        audioPlayer.prepareToPlay()
        if playState == true {
            audioPlayer.play()
            playPauseButton.setTitle("Pause", for: .normal)
        } else {
            playPauseButton.setTitle("Play", for: .normal)
        }
    }
    
    // Updates the slider to the current time of the audio.
    // This will be called every one second repeatedly till the audio is over.
    @objc func updateSlider() {
        slider.value = Float(audioPlayer.currentTime)
        currentTime.text = getTime(time: audioPlayer.currentTime)
        progress.setProgress(slider.value / slider.maximumValue, animated: true)
        
    }
    
    // Returns the time in the format min:seconds for the time interval
    func getTime(time: TimeInterval) -> String{
        let ti = NSInteger(time)
        let mins = String((ti / 60) % 60)
        let seconds = ti % 60
        var stringSeconds : String
        if seconds < 10 {
            stringSeconds = "0" + String(seconds)
        } else {
            stringSeconds = String(seconds)
        }
        let stamp: String = mins + ":" + stringSeconds
        return stamp
    }
}
