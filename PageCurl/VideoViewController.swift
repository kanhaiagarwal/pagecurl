//
//  VideoViewController.swift
//  PageCurl
//
//  Created by Kumar Agarwal, Kanhai on 02/06/18.
//  Copyright © 2018 Kumar Agarwal, Kanhai. All rights reserved.
//

import UIKit
import AVKit

class VideoViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func playOnClick(_ sender: UIButton) {
        
        // Store the path of the video inside a variable if it exists.
        if let path = Bundle.main.path(forResource: "WhatsApp Video 2018-06-02 at 12.47.13 AM", ofType: "mp4") {
            let video = AVPlayer(url: URL(fileURLWithPath: path))
            let videoPlayer = AVPlayerViewController()
            videoPlayer.player = video
            
            // Open the video view controller.
            present(videoPlayer, animated: true, completion: {
                video.play()
            })
        }
    }
    
}
