//
//  StoryImagesViewController.swift
//  PageCurl
//
//  Created by Kumar Agarwal, Kanhai on 20/05/18.
//  Copyright © 2018 Kumar Agarwal, Kanhai. All rights reserved.
//

import UIKit

class StoryImagesViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    var images = ["DSC_2514.jpg", "IMG_20170909_231341.jpg", "IMG_20171209_211937.jpg"]
    
    var i : Int = 0
    
    var nextBtn : UIButton = {
        let btn = UIButton()
        btn.setTitle("->", for: .normal)
        btn.isHidden = true
        return btn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView.isUserInteractionEnabled = true
        imageView.image = UIImage(named: images[0])
        
        let closeBtn = UIButton(frame: CGRect(x: 50, y: 50, width: 30, height: 30))
        closeBtn.setTitle("X", for: .normal)
        
        nextBtn.frame = CGRect(x: view.frame.width - 50, y: 50, width: 50, height: 50)
        nextBtn.isEnabled = false
        
        let nextClick = UITapGestureRecognizer(target: self, action: #selector(StoryImagesViewController.onNextClick))
        nextBtn.addGestureRecognizer(nextClick)
        
        view.addSubview(closeBtn)
        
        view.addSubview(nextBtn)
        
    }
    
    @objc func onNextClick() {
        performSegue(withIdentifier: "gotoAudio", sender: self)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("Inside touches began")
        super.touchesBegan(touches, with: event)
        if let touch = touches.first {
            print("Inside touch = touches.first")
            if touch.view == imageView {
                print("Inside touch.view == imageView")
                let location = touch.location(in: imageView)
                if location.y > imageView.frame.height / 2 {
                    nextImage()
                } else {
                    previousImage()
                }
            }
        }
    }
    
    func nextImage() {
        if i < images.count - 1 {
            i += 1
            imageView.image = UIImage(named: images[i])
            
            UIView.transition(with: imageView, duration: 0.5, options: .transitionCurlUp, animations: nil, completion: { (completed) in
                if self.i == self.images.count - 1 {
                    self.nextBtn.isHidden = false
                    self.nextBtn.isEnabled = true
                }
            })
        }
    }
    
    func previousImage() {
        nextBtn.isHidden = true
        nextBtn.isEnabled = false
        if i > 0 {
            i -= 1
            imageView.image = UIImage(named: images[i])
            
            UIView.transition(with: imageView, duration: 0.5, options: .transitionCurlDown, animations: nil, completion: nil)
        }
    }
    
}
