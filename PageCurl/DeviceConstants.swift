//
//  DeviceConstants.swift
//  PageCurl
//
//  Created by Kumar Agarwal, Kanhai on 22/05/18.
//  Copyright © 2018 Kumar Agarwal, Kanhai. All rights reserved.
//

import Foundation
import UIKit

public class DeviceConstants {
    static public let IPHONE5S_HEIGHT : CGFloat = 568.0
    static public let IPHONE7_HEIGHT : CGFloat = 667.0
    static public let IPHONE7PLUS_HEIGHT : CGFloat = 736.0
    static public let IPHONEX_HEIGHT : CGFloat = 812.0
}
