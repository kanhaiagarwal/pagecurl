//
//  SwipeImagesViewController.swift
//  PageCurl
//
//  Created by Kumar Agarwal, Kanhai on 25/05/18.
//  Copyright © 2018 Kumar Agarwal, Kanhai. All rights reserved.
//

import UIKit

class SwipeImagesViewController: UIViewController, UIGestureRecognizerDelegate {
    
    let changeAngle : Float = Float(CGFloat.pi / 2500)
    var divisor : CGFloat!
    var images = [UIImage]()
    var cards = [ImageCard]()
    var cardOutside : Int = -1
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadImageArray()
        setViews()
    }
    
    // Creates cards for each image, and adds them in the main view.
    // The first view to be added is the last image uploaded by the user.
    // The view on the top will be the first image uploaded by the user.
    // Give each view a tag. This will be helpful in sliding back the image when its outside of the view.
    // Give each view a swipe gesture through UIPanGestureRecognizer.
    func setViews() {
        for i in (0 ..< images.count) {
            cards.append(ImageCard(frame: CGRect(x: view.frame.width / 4, y: view.frame.height / 4, width: view.frame.width, height: view.frame.height / 2)))
            cards[i].imageView.image = images[images.count - 1 - i]
            cards[i].center = view.center
            cards[i].tag = i
            if cards[i].tag % 2 == 0 {
                cards[i].transform = CGAffineTransform(rotationAngle: CGFloat.pi / 40)
            } else {
                cards[i].transform = CGAffineTransform(rotationAngle: -CGFloat.pi / 40)
            }
            let swipeGesture = UIPanGestureRecognizer(target: self, action: #selector(self.onSwipeCard(sender:)))
            cards[i].addGestureRecognizer(swipeGesture)
            view.addSubview(cards[i])
        }
        
        let swipeViewGesture = UIPanGestureRecognizer(target: self, action: #selector(self.onSwipeView(sender:)))
        view.addGestureRecognizer(swipeViewGesture)
    }
    
    func loadImageArray() {
        images = [UIImage(named: "Fifth2-1.jpg")!, UIImage(named: "Fifth2")!, UIImage(named: "group_mic.jpg")!, UIImage(named: "o-FRIENDSHIP-facebook")!, UIImage(named: "Untitled-2.jpg")!]
        cardOutside = images.count
        
    }
    
    @objc func onSwipeCard(sender: UIPanGestureRecognizer) {
        
        let tag = sender.view!.tag
        
        // This gives the point relative to the point where the touch first happended
        let point = sender.translation(in: view)
        
        // When the user has swiped left
        if point.x < 0 {
            cards[tag].center.x = view.center.x + point.x
        }
        
        if sender.state == .ended {
            if cards[tag].center.x < view.center.x / 20 && cards[tag].tag > 0 {
                UIView.animate(withDuration: 0.3, animations: {
                    self.cards[tag].center = CGPoint(x: -self.view.center.x, y: self.self.cards[tag].center.y)
                    self.self.cards[tag].alpha = 0
                })
                cardOutside = tag
            } else if point.x > 0 && cardOutside < images.count && cardOutside >= 0 {
                UIView.animate(withDuration: 0.2, animations: {
                    self.cards[self.cardOutside].center = self.view.center
                    self.self.cards[self.cardOutside].alpha = 1
                    self.cardOutside += 1
                })
            } else {
                UIView.animate(withDuration: 0.2, animations: {
                    self.cards[tag].center = self.view.center
                })
            }
        }
    }
    
    @objc func onSwipeView(sender: UIPanGestureRecognizer) {
        
        let point = sender.translation(in: view)
        
        // When the finger is swiped to the right of the point where the touch first came
        if sender.state == .ended {
            if point.x > 0 && cardOutside < images.count && cardOutside >= 0 {
                UIView.animate(withDuration: 0.2, animations: {
                    self.cards[self.cardOutside].center = self.view.center
                    self.cards[self.cardOutside].alpha = 1
                    self.cardOutside += 1
                })
            }
        }
    }
    
}
