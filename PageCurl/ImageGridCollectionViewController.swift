//
//  ImageGridCollectionViewController.swift
//  PageCurl
//
//  Created by Kumar Agarwal, Kanhai on 10/06/18.
//  Copyright © 2018 Kumar Agarwal, Kanhai. All rights reserved.
//

import UIKit
import Photos

private let reuseIdentifier = "Cell"

class ImageGridCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    var allImagesArray = [UIImage]()
    var selectedImagesMap = [Int : UIImage]()
    
    let maxImagesToBeSelected : Int = 3
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView?.allowsMultipleSelection = true
        grabPhotos()
    }

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allImagesArray.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
        var newFrame : CGRect = cell.frame
        newFrame.size = CGSize(width: view.frame.width / 3, height: view.frame.width / 3)
        cell.frame = newFrame;
    
        let imageView = cell.viewWithTag(1) as! UIImageView
        imageView.image = allImagesArray[indexPath.row]
    
        return cell
    }
    
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        let cell = collectionView.cellForItem(at: indexPath)
        
        let imageView = cell?.viewWithTag(1) as! UIImageView
        
        if selectedImagesMap.count < maxImagesToBeSelected {
            selectedImagesMap[indexPath.row] = imageView.image
            
            cell?.layer.borderColor = UIColor.green.cgColor
            cell?.layer.borderWidth = 3
            print("selectedImagesMap: \(String(selectedImagesMap.debugDescription))")
            return true
        } else {
            cell?.isSelected = false
            print("selectedImagesMap: \(String(selectedImagesMap.debugDescription))")
            return false
        }
        
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, shouldDeselectItemAt indexPath: IndexPath) -> Bool {
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.layer.borderWidth = 0
        
        selectedImagesMap.removeValue(forKey: indexPath.row)
        print("selectedImagesMap: \(String(selectedImagesMap.debugDescription))")
        
        return true
    }
    
    func grabPhotos() {
        let imgManager = PHImageManager.default()
        
        // What mode and quality in which we want the images from the image manager.
        let requestOptions = PHImageRequestOptions()
        requestOptions.isSynchronous = true
        requestOptions.deliveryMode = .highQualityFormat
        
        // Fetch options to get the configure the sequence in which the photos come.
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        
        let fetchResult: PHFetchResult = PHAsset.fetchAssets(with: .image, options: fetchOptions)
        if fetchResult.count > 0 {
            for i in 0..<fetchResult.count {
                imgManager.requestImage(for: fetchResult.object(at: i), targetSize: CGSize(width: 100, height: 100), contentMode: .aspectFill, options: requestOptions, resultHandler: { (image, error) in
                        print(image!.description)
                        self.allImagesArray.append(image!)
                })
            }
            self.collectionView?.reloadData()
        }
    }
    
    
    // Determine the Size of the cell in the collection view
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width / 3 - 1
        
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }

}
