//
//  ImageCard.swift
//  PageCurl
//
//  Created by Kumar Agarwal, Kanhai on 30/05/18.
//  Copyright © 2018 Kumar Agarwal, Kanhai. All rights reserved.
//

import UIKit

@IBDesignable class ImageCard: UIView {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageText: UITextView!
    @IBInspectable var cornerRadius : CGFloat = 10
    @IBInspectable var shadowWidth : CGFloat = 1
    @IBInspectable var shadowHeight : CGFloat = 1
    @IBInspectable var shadowOpacity : CGFloat = 0.2
    @IBInspectable var shadowColor : UIColor = UIColor.black
    
    
    var view: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override func layoutSubviews() {
        layer.cornerRadius = cornerRadius
        layer.shadowColor = shadowColor.cgColor
        layer.shadowOffset = CGSize(width: shadowWidth, height: shadowHeight)
//        layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
        layer.shadowOpacity = Float(shadowOpacity)
    }
    
    func setup() {
        view = loadFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        addSubview(view)
    }
    
    func loadFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "ImageCard", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }

}
